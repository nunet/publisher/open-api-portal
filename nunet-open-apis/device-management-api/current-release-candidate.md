# Current Release Candidate

Automatically built from staging branch [https://gitlab.com/nunet/open-api/device-management-api-spec/-/tree/staging?ref\_type=heads](https://gitlab.com/nunet/open-api/device-management-api-spec/-/tree/staging?ref\_type=heads)

* [https://nunet.gitlab.io/open-api/device-management-api-spec/staging/async-api/](https://nunet.gitlab.io/open-api/device-management-api-spec/staging/async-api/)
* [https://nunet.gitlab.io/open-api/device-management-api-spec/staging/swagger/](https://nunet.gitlab.io/open-api/device-management-api-spec/staging/swagger/)
