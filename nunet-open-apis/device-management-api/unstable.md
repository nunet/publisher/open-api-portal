# Unstable

Automatically built from the current state of the develop branch [https://gitlab.com/nunet/open-api/device-management-api-spec/-/tree/develop?ref\_type=heads](https://gitlab.com/nunet/open-api/device-management-api-spec/-/tree/develop?ref\_type=heads)

* [https://nunet.gitlab.io/open-api/device-management-api-spec/develop/async-api/](https://nunet.gitlab.io/open-api/device-management-api-spec/develop/async-api/)
* [https://nunet.gitlab.io/open-api/device-management-api-spec/develop/swagger/](https://nunet.gitlab.io/open-api/device-management-api-spec/develop/swagger/)

