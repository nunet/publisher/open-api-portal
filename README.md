# NuNet Open API

We are exposing all functionality of the NuNet platform (and each component) via a collection of Open APIs. Core platform functionalities will be implemented via these APIs and interaction between them. All APIs will be open to use both internally and externally, but enabling private data encryption.

We will be publishing API descriptions for each version of the platform as well as maintain the history. Please note however that this is a work in progress and currently API descriptions may not be fully synchronised with the code. In the future, this portal will be automatically updated and synced with platfrom codebase on [https://gitlab.com/nunet](https://gitlab.com/nunet) and open-api descriptions on [https://gitlab.com/nunet/open-api](https://gitlab.com/nunet/open-api).

The progress and future of NuNet Open-APIs is tracked via Open API of APIs management epic at [https://gitlab.com/groups/nunet/-/epics/19](https://gitlab.com/groups/nunet/-/epics/19).
